﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MenuControler : MonoBehaviour {
    public GameObject start, credits, exit;
    public GameObject hud, hud_start;
    public GameObject player;
    public GameObject plate;
    public List<GameObject> waves;
    public GameObject agradecimento;
    public List<GameObject> sceneObjs = new List<GameObject>();

    public GameObject credits_objs;

    public Text startTextGame;

    public void exitGame()
    {
        Application.Quit();
    }

    public void creditsGame() {
        startTextGame.enabled = false;
        plate.GetComponent<Rotate>().initializaPlateMove();
        foreach (GameObject obj in sceneObjs)
            obj.SetActive(false);

        credits_objs.SetActive(true);
        agradecimento.SetActive(true);
    }

    void Start() {
        
        AudioManager.instance.PlayMusic("Sounds/bgm");
    }

    public void startGame()
    {
        startTextGame.enabled = false;
        for (int i = 0; i < waves.Count; i++)
            waves[i].GetComponent<WaveManager>().start = true;
        
        plate.GetComponent<Rotate>().initializaPlateMove();
        agradecimento.SetActive(false);
        hud_start.SetActive(false);
        hud.SetActive(true);

        credits_objs.SetActive(false);

        foreach (GameObject obj in sceneObjs)
            obj.SetActive(true);

        player.GetComponent<Player>().allowInput = true;
        start.GetComponent<Button>().interactable = false;
        credits.GetComponent<Button>().interactable = false;
    }

    void Update() {
        if (!hud.activeInHierarchy)
            if (Input.GetKeyUp(KeyCode.Return) || Input.GetKeyUp(KeyCode.Space))
                startGame();
               
    }


    public void restartScene()
    {
        Application.LoadLevel(Application.loadedLevel);
    }

    public void nextLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
    }
}
