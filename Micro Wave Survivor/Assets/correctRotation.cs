﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class correctRotation : MonoBehaviour {
    
	// Update is called once per frame
	void Update () {
        float x = transform.eulerAngles.x;
        float y = transform.eulerAngles.y;
        float z = transform.eulerAngles.z;
        transform.eulerAngles = new Vector3(x, 0, z);
	}
}
