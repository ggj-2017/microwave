﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class timeControler : MonoBehaviour {
    public bool timeIsOver = false;
    public int min = 2;
    public float sec = 59;
    public Text mins, dots, secs;

    bool stopped = false;

    bool four, three, two, one;
    bool tocoufour, tocouthree, tocoutwo, tocouone;

    float startTime, dotTime = 1.0f;

    void Start() {
        mins.text = ((int)min).ToString();
        secs.text = ((int)sec).ToString();

        four = false;
        three = false;
        two = false;
        one = false;

        tocoufour = false;
        tocouthree = false;
        tocoutwo = false;
        tocouone = false;
    }

    void Update() {
        endDotSound();

        if (!stopped) {
            sec -= Time.deltaTime;
            dotTime -= Time.deltaTime;
        }

        if (sec <= 0.0 && min > 0) {
            min -= 1;
            sec = 59;
        }
        if (min <= 0 && sec <= 0) {
            sec = 0;
            min = 0;
            timeIsOver = true;
            //GameOver
        }

        if (sec < 10)
            secs.text = "0" + ((int)sec).ToString();
        else
            secs.text = ((int)sec).ToString();

        
        if(min < 10)
            mins.text = "0" + ((int)min).ToString();
        else
            mins.text = ((int)min).ToString();

        if (dotTime < 0.0) {
            if (dots.gameObject.activeInHierarchy)
                dots.gameObject.SetActive(false);
            else
                dots.gameObject.SetActive(true);

            dotTime = 1.0f;
        }

    }

    public void stop() {
        stopped = true;
        sec = 0;
        min = 0;
    }

    void endDotSound()
    {
        if ((int)sec == 4 && !tocoufour)
            four = true;
        if ((int)sec == 3 && !tocouthree)
            three = true;
        if ((int)sec == 2 && !tocoutwo)
            two = true;
        if ((int)sec == 1 && !tocouone)
            one = true;

        if (four)
        {
            AudioManager.PlaySound("Sounds/sfx_microOnline", 0.3f);
            four = false;
            tocoufour = true;
        }
        else if (three)
        {
            AudioManager.PlaySound("Sounds/sfx_microOnline", 0.3f);
            three = false;
            tocouthree = true;
        }
        else if (two)
        {
            AudioManager.PlaySound("Sounds/sfx_microOnline", 0.3f);
            two = false;
            tocoutwo = true;
        }
        else if (one)
        {
            AudioManager.PlaySound("Sounds/sfx_microOnline", 0.3f);
            one = false;
            tocouone = true;
        }
    }

    }
