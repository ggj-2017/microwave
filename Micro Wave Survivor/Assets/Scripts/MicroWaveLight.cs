﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MicroWaveLight : MonoBehaviour
{
    public List<Light> lights;

	void Start ()
    {
		
	}
	
	void Update ()
    {
        if (Input.GetKey(KeyCode.Alpha1))
            ChangeColor(1);
        if (Input.GetKey(KeyCode.Alpha2))
            ChangeColor(2);
        if (Input.GetKey(KeyCode.Alpha3))
            ChangeColor(3);
        if (Input.GetKey(KeyCode.Alpha4))
            ChangeColor(4);
        if (Input.GetKey(KeyCode.Alpha5))
            ChangeColor(5);
        if (Input.GetKey(KeyCode.Alpha6))
            ChangeColor(6);
        if (Input.GetKey(KeyCode.Alpha7))
            ChangeColor(7);
        if (Input.GetKey(KeyCode.Alpha8))
            ChangeColor(8);
        if (Input.GetKey(KeyCode.Alpha9))
            ChangeColor(9);
        if (Input.GetKey(KeyCode.Alpha0))
            ChangeColor(0);
    }

    public void ChangeColor(int id)
    {
        for (int i = 0; i < lights.Count; i++)
        {
            switch (id)
            {
                case 0:
                    Color col = new Color(1.0f, 0.5f, 0);
                    lights[i].color = col;
                    break;
                case 1:
                    lights[i].color = Color.red;
                    break;
                case 2:
                    lights[i].color = Color.blue;
                    break;
                case 3:
                    lights[i].color = Color.green;
                    break;
                case 4:
                    lights[i].color = Color.magenta;
                    break;
                case 5:
                    lights[i].color = Color.yellow;
                    break;
                case 6:
                    lights[i].color = Color.white;
                    break;
                case 7:
                    lights[i].color = Color.cyan;
                    break;
                case 8:
                    Color col2 = new Color(0.7f, 0.5f, 0.4f);
                    lights[i].color = col2;
                    break;
                case 9:
                    lights[i].color = Color.gray;
                    break;
                default:
                    break;
            }
        }
    }
}
