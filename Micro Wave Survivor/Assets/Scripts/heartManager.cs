﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class heartManager : MonoBehaviour {
    public int lifes = 3;
    public List<GameObject> hearts = new List<GameObject>();
    public List<GameObject> hearts_empty = new List<GameObject>();

    public void decreaseLife() {
        if(lifes == 3)
            AudioManager.PlaySound("Sounds/sfx_damage", .5f);
        if(lifes == 2)
            AudioManager.PlaySound("Sounds/sfx_damage1", .5f);
        if (lifes == 1)
            AudioManager.PlaySound("Sounds/sfx_damage2", .5f);


        lifes--;
        if (lifes >= 0)
        {
            foreach (GameObject obj in hearts)
                if (obj.activeInHierarchy)
                {
                    obj.SetActive(false);
                    break;
                }

            foreach (GameObject obj in hearts_empty)
                if (!obj.activeInHierarchy)
                {
                    obj.SetActive(true);
                    break;
                }
        }
    }

    public void increaseLife() {
        lifes++;
        foreach (GameObject obj in hearts)
            if (!obj.activeInHierarchy) {
                obj.SetActive(true);
                break;
            }

        foreach (GameObject obj in hearts_empty)
            if (obj.activeInHierarchy) {
                obj.SetActive(false);
                break;
            }

    }

}
