﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotonCannon : MonoBehaviour {

    public GameObject photon;
    public GameObject launcher;
    
    public float ballMoveSpeed = 0.4f;
    public float ballAmplitude = 0.5f;
    public float ballFrequency = 40.0f;
    public float ballSelfRotationSpeed = 300.0f;
    
    public Color trailColor = Color.white;
    public bool randColor = false;
    public float trailLifeTime = 5;
    public float trailSize = 0.2f;

    public float angularRange = 20.0f;
    
    void Start ()
    {
		
	}
	
	void Update ()
    {
        //if (Input.GetKeyDown(KeyCode.Space))
        //    LaunchPhoton();
	}

    public void LaunchPhoton()
    {
        int i = Random.Range(0, 3);
        AudioManager.PlaySound("Sounds/sfx_shoot"+i, 0.3f);
        GameObject go = Instantiate(photon);

        SetPhotonProperties(go);

        go.transform.SetParent(this.gameObject.transform);
        go.transform.localPosition = launcher.transform.localPosition;
        go.transform.localEulerAngles = new Vector3(95, go.transform.localEulerAngles.y, go.transform.localEulerAngles.z);
        
        this.GetComponentInChildren<ParticleSystem>().Play();
    }

    void SetPhotonProperties(GameObject go)
    {
        go.GetComponentInChildren<PhotonManager>().MoveSpeed = ballMoveSpeed;
        go.GetComponentInChildren<PhotonManager>().frequency = ballFrequency;
        go.GetComponentInChildren<PhotonManager>().amplitude = ballAmplitude;
        go.GetComponentInChildren<PhotonManager>().myRotationAngleSpeed = ballSelfRotationSpeed;
        go.GetComponentInChildren<PhotonManager>().angularRange = angularRange;

        if (randColor)
        {
            Color col = new Color();
            col = Color.HSVToRGB(Random.Range(0,1.0f), Random.Range(0, 1.0f), Random.Range(0, 1.0f));
            go.GetComponentInChildren<PhotonManager>().trailColor = col;
        } else
            go.GetComponentInChildren<PhotonManager>().trailColor = trailColor;

        go.GetComponentInChildren<PhotonManager>().trailLifeTime = trailLifeTime;
        go.GetComponentInChildren<PhotonManager>().trailSize = trailSize;
    }
}
