﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class waveControler : MonoBehaviour {
    public int startValue = 0;
    public Text waveNum;

    void Start() {
        int id = SceneManager.GetActiveScene().buildIndex;
        

        waveNum.text = id.ToString();
    }

    public void nextWave() {
        int num = int.Parse(waveNum.text)+1;
        waveNum.text = num.ToString();
    }
}
