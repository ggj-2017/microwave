﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotonShadow : MonoBehaviour {

    public SpriteRenderer shadow;
    
    void Update()
    {
        RaycastHit hit;

        if (Physics.Raycast(transform.position, -Vector3.up, out hit))
        {
            shadow.transform.position = new Vector3(hit.point.x, hit.point.y+0.05f, hit.point.z);
            shadow.transform.eulerAngles = new Vector3(270, 0, 0);

            //shadow.transform.localScale = new Vector3(shadow.transform.localScale.x/hit.distance, shadow.transform.localScale.y / hit.distance, shadow.transform.localScale.z / hit.distance);
        }
    }
}
