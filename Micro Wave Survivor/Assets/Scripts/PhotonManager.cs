﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PhotonManager : MonoBehaviour
{
    public float MoveSpeed = 0.4f;
    public float amplitude = 0.5f;
    public float frequency = 40.0f;

    public TrailRenderer trail;
    public Color trailColor;
    public float trailLifeTime = 5;
    public float trailSize = 0.2f;

    private Vector3 pos;
    private float yRefPos;
    private float direction;

    public float myRotationAngleSpeed = 0;
    private float rotationZ = 0;

    public float angularRange = 20;

    void Start()
    {
        CreateTrail();

        pos = transform.position;
        yRefPos = pos.y;

        direction = Random.Range(-angularRange, angularRange) + transform.localEulerAngles.y -90;

    }

    void Update()
    {
        rotationZ += myRotationAngleSpeed * Time.deltaTime;
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, 0, rotationZ);

        Move();
    }

    void CreateTrail()
    {
        //var object= new GameObject.CreatePrimitive(PrimitiveType.Sphere);
        trail = transform.gameObject.AddComponent<TrailRenderer>();
        Material temp_material = new Material(Shader.Find("Particles/Alpha Blended Premultiply"));
        trail.material = temp_material;

        trail.startWidth = trailSize;
        trail.endWidth = 0.1f;
        trail.time = trailLifeTime;

        //end color alfa is set to 0
        Color endColor = trailColor;
        endColor.a = 0;
    
        trail.startColor = trailColor;
        trail.endColor = endColor;

        this.GetComponent<SpriteRenderer>().color = trail.startColor;
    }

    void Move()
    {
        pos.x += Mathf.Cos(direction * Mathf.Deg2Rad) * MoveSpeed * Time.deltaTime;
        pos.z += Mathf.Sin(direction * Mathf.Deg2Rad) * MoveSpeed * Time.deltaTime;
        
        pos.y = yRefPos + Mathf.Sin(Time.time * frequency) * amplitude;

        transform.position = pos;
    }

    void OnTriggerEnter(Collider other)
    {
        Debug.Log("Entrou no " + other.tag);

        if (other.CompareTag("wall"))
        {
            AudioManager.PlaySound("Sounds/sfx_explosion", 0.02f);
            direction += 180 + 45;
        }
        else if (other.CompareTag("Player"))
        {
            other.GetComponent<Player>().life--;
            other.GetComponent<Player>().GUIHearth.GetComponent<heartManager>().decreaseLife();

            if (other.GetComponent<Player>().life <= 0)
            {
                //TODO TERMINAR JOGO AQUI
                int i = Random.Range(0, 2);
                AudioManager.PlaySound("Sounds/sfx_dead" + i, .5f);
            }

            Destroy(this.gameObject);
        }
        else if (other.CompareTag("Food"))
        {
            AudioManager.PlaySound("Sounds/sfx_explosion", 0.5f);

            Destroy(this.gameObject);
            other.GetComponent<FoodManager>().life--;

            other.GetComponent<FoodManager>().ManageSprites();
        }
    }
}
