﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Collections.Generic;

public class FoodManager : MonoBehaviour
{
    public int life = 5;

    public List<Sprite> sprites;

    public int numOfLifesToChangeSprite = 0;
    public int auxNumOfLifesToChangeSprite = 0;
    public int countSprites;

    Rigidbody rb;

	void Start ()
    {
        rb = GetComponent<Rigidbody>();
        numOfLifesToChangeSprite = life / sprites.Count;
    }
	
	void Update ()
    {
		
	}

    public void ManageSprites()
    {
        auxNumOfLifesToChangeSprite++;

        if (auxNumOfLifesToChangeSprite >= numOfLifesToChangeSprite)
        {
            if (life > 0)
                GetComponent<SpriteRenderer>().sprite = sprites[countSprites];
            else
            {
                GetComponent<SpriteRenderer>().color = Color.black;

                GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionZ;
                tag = "DeadFood";
            }

            countSprites++;
        }
    }
    void OnCollisionExit(Collision col) {
        if (col.gameObject.CompareTag("Player")) {
            GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
        }
    }
    
}
