﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    public bool start = false;
    public bool generateByRange = false;

    public float totalTimeToGenerate = 45.0f;
    public float timeToGenerate = 1.5f;

    public int maxOfPhotons = 15;
    private int countOfPhotons = 0;
    
    private float auxTimeToGenerate = 0;
    private float timeRunning = 0;

    public bool startShooting = false;
    public bool randStartShooting = false;

    void Start()
    {
        if (randStartShooting)
        {
            int rnd = Random.Range(0, 2);
            if (rnd == 0)
                startShooting = true;
            else
                startShooting = false;
        }

        if (startShooting)
            auxTimeToGenerate = 10000;
    }
	
	void Update ()
    {
        if (start)
        {
            if (timeRunning < totalTimeToGenerate)
            {
                timeRunning += Time.deltaTime;
                TimeManagerToShoot();
            }
        }
	}

    void TimeManagerToShoot()
    {
        if(generateByRange)
        {
            if (auxTimeToGenerate < timeToGenerate + Random.Range(0.0f, 1.5f))
                auxTimeToGenerate += Time.deltaTime;
            else
            {
                auxTimeToGenerate = 0;

                if (countOfPhotons < maxOfPhotons)
                {
                    this.GetComponent<PhotonCannon>().LaunchPhoton();
                    countOfPhotons++;
                }
            }
        }
        else
        {
            if (auxTimeToGenerate < timeToGenerate)
                auxTimeToGenerate += Time.deltaTime;
            else
            {
                auxTimeToGenerate = 0;

                if (countOfPhotons < maxOfPhotons)
                {
                    this.GetComponent<PhotonCannon>().LaunchPhoton();
                    countOfPhotons++;
                }
            }
        }
    }
}
