﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class loseAnimationManager : MonoBehaviour
{
    public List<GameObject> animations;

    public Transform initialPos;
    public Transform finalPos;
    public float speed = 300;

	void Start ()
    {
		
	}
	
	void Update ()
    {
        if (Input.GetKeyUp(KeyCode.Z))
            StartCoroutine(MoveAnimations());
    }

    IEnumerator MoveAnimations()
    {
        for (int i = 0; i < animations.Count; i++)
        {
            Debug.Log("Entrou AQUEE");
            Vector3 aux = animations[i].transform.localPosition;
            Vector3 newPos = Vector3.MoveTowards(animations[i].transform.localPosition, finalPos.position, speed * Time.deltaTime);

            animations[i].transform.localPosition = new Vector3(aux.x, newPos.y, aux.z);

            yield return new WaitForSeconds(0.2f);
        }
    }
}
