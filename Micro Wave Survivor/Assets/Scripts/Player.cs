﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Player : MonoBehaviour
{
    private bool isEndOfGame = false;

    public GameObject GUIHearth;
    public GameObject time;

    public float speed = 0f;
    public int life = 3;

    Rigidbody rb;

    public bool allowInput = true;
    
    Vector3 respawPoint;

    public GameObject interaction;

    public GameObject win, loose;

    void Start() {
        rb = GetComponent<Rigidbody>();
        respawPoint = transform.position;
        //allowInput = false;
    }

    void Update() {
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, 0, transform.eulerAngles.z);

        if (allowInput) {
            moveArrows();
            moveWASD();
        }

        if (time.GetComponent<timeControler>().timeIsOver && GUIHearth.GetComponent<heartManager>().lifes > 0 && !isEndOfGame) {
            time.GetComponent<timeControler>().stop();
            allowInput = false;
            win.SetActive(true);
            isEndOfGame = true;

            AudioManager.PlaySound("Sounds/sfx_gameover", 0.5f);
            gameObject.SetActive(false);
            //win the game
        }

        if (GUIHearth.GetComponent<heartManager>().lifes <= 0 && !isEndOfGame) {
            
            time.GetComponent<timeControler>().stop();
            allowInput = false;
            loose.SetActive(true);
            gameObject.SetActive(false);
        }

    }

    void moveArrows()
    {
        float x, y, z;
        x = transform.position.x;
        y = transform.position.y;
        z = transform.position.z;

        float step = speed * Time.deltaTime;

       

        if (Input.GetKey(KeyCode.UpArrow)) {
            Debug.Log("aqui");
            //transform.Translate(Vector3.forward * step);
            rb.velocity = Vector3.forward * speed;
            GetComponent<Animator>().SetBool("caminhadat", true);
        }
        if (Input.GetKeyUp(KeyCode.UpArrow)) {
            GetComponent<Animator>().SetBool("caminhadat", false);
            rb.velocity = Vector3.zero;
        }


        if (Input.GetKey(KeyCode.DownArrow)) {
            //transform.Translate(Vector3.back * step);
            rb.velocity = Vector3.back * speed;
            GetComponent<Animator>().SetBool("caminhadaf", true);
        }
        if (Input.GetKeyUp(KeyCode.DownArrow)) {
            GetComponent<Animator>().SetBool("caminhadaf", false);
            rb.velocity = Vector3.zero;
        }


        if (Input.GetKey(KeyCode.RightArrow))
        {
            //transform.Translate(Vector3.right * step);
            rb.velocity = Vector3.right * speed;
            GetComponent<SpriteRenderer>().flipX = false;
            GetComponent<Animator>().SetBool("caminhadal", true);
        }
        if (Input.GetKeyUp(KeyCode.RightArrow)) {
            GetComponent<Animator>().SetBool("caminhadal", false);
            rb.velocity = Vector3.zero;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            //transform.Translate(Vector3.left * step);
            rb.velocity = Vector3.left * speed;
            GetComponent<SpriteRenderer>().flipX = true;
            GetComponent<Animator>().SetBool("caminhadal", true);
        }
        if (Input.GetKeyUp(KeyCode.LeftArrow)) {
            GetComponent<Animator>().SetBool("caminhadal", false);
            rb.velocity = Vector3.zero;
        }



        //Hit lateral
        if (Input.GetKey(KeyCode.V)) {
            if (GetComponent<SpriteRenderer>().flipX)
                GetComponent<SpriteRenderer>().flipX = false;

            GetComponent<Animator>().SetBool("hitl", true);
        }

        if (Input.GetKeyUp(KeyCode.V))
            GetComponent<Animator>().SetBool("hitl", false);

        //hit lateral ao contrario
        if (Input.GetKey(KeyCode.B)) {
            if(!GetComponent<SpriteRenderer>().flipX)
                GetComponent<SpriteRenderer>().flipX = true;

            GetComponent<Animator>().SetBool("hitl", true);
        }

        if (Input.GetKeyUp(KeyCode.B)) {
            //GetComponent<SpriteRenderer>().flipX = false;
            GetComponent<Animator>().SetBool("hitl", false);
        }
        //hitFrontal
        if (Input.GetKey(KeyCode.N))
        {
            GetComponent<Animator>().SetBool("hitf", true);
        }

        if (Input.GetKeyUp(KeyCode.N))
        {
            GetComponent<Animator>().SetBool("hitf", false);
        }
        //hitTraseiro
        if (Input.GetKey(KeyCode.M))
        {
            GetComponent<Animator>().SetBool("hitt", true);
        }

        if (Input.GetKeyUp(KeyCode.M))
        {
            GetComponent<Animator>().SetBool("hitt", false);
        }


    }

    void moveWASD() {
        float x, y, z;
        x = transform.position.x;
        y = transform.position.y;
        z = transform.position.z;

        float step = speed * Time.deltaTime;

        if (Input.GetKey(KeyCode.W))
        {
            //transform.Translate(Vector3.forward * step);
            rb.velocity = Vector3.forward * speed;
            GetComponent<Animator>().SetBool("caminhadat", true);
        }
        if (Input.GetKeyUp(KeyCode.W))
        {
            GetComponent<Animator>().SetBool("caminhadat", false);
            rb.velocity = Vector3.zero;
        }


        if (Input.GetKey(KeyCode.S))
        {
            //transform.Translate(Vector3.back * step);
            rb.velocity = Vector3.back * speed;
            GetComponent<Animator>().SetBool("caminhadaf", true);
        }
        if (Input.GetKeyUp(KeyCode.S))
        {
            GetComponent<Animator>().SetBool("caminhadaf", false);
            rb.velocity = Vector3.zero;
        }


        if (Input.GetKey(KeyCode.D))
        {
            //transform.Translate(Vector3.right * step);
            rb.velocity = Vector3.right * speed;
            GetComponent<SpriteRenderer>().flipX = false;
            GetComponent<Animator>().SetBool("caminhadal", true);
        }
        if (Input.GetKeyUp(KeyCode.D))
        {
            GetComponent<Animator>().SetBool("caminhadal", false);
            rb.velocity = Vector3.zero;
        }

        if (Input.GetKey(KeyCode.A))
        {
            //transform.Translate(Vector3.left * step);
            rb.velocity = Vector3.left * speed;
            GetComponent<SpriteRenderer>().flipX = true;
            GetComponent<Animator>().SetBool("caminhadal", true);
        }
        if (Input.GetKeyUp(KeyCode.A))
        {
            GetComponent<Animator>().SetBool("caminhadal", false);
            rb.velocity = Vector3.zero;
        }
    }

    void respawPlayer() {
        transform.position = respawPoint;
    }

    void OnCollisionEnter(Collision col) {
        
        if (col.gameObject.CompareTag("Food") && col.gameObject.tag != "DeadFood") {
            interaction.SetActive(true);
        }
    }

    void OnCollisionStay(Collision col) {
        if (col.gameObject.CompareTag("Food") && interaction.activeInHierarchy) {
            if (Input.GetKeyUp(KeyCode.Space) && col.gameObject.tag != "DeadFood")
            {
                AudioManager.PlaySound("Sounds/sfx_comendo", 0.5f);
                Destroy(col.gameObject);
                interaction.SetActive(false);
                GUIHearth.GetComponent<heartManager>().increaseLife();
            }
        }

    }

    void OnCollisionExit(Collision col) {
        if (col.gameObject.CompareTag("Food") && interaction.activeInHierarchy){
            interaction.SetActive(false);
        }
    }


}
