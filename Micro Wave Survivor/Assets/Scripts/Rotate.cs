﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rotate : MonoBehaviour {

    public float speed = 15.0f;
    bool initializeMove = false;

    void Start()
    {
        int rand = Random.Range(0, 2);
        if (rand == 1)
            speed *= -1;
    }

    void Update()
    {
        if(initializeMove)
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y + speed*Time.deltaTime, transform.eulerAngles.z);

    }

    public void initializaPlateMove() {
        initializeMove = true;
    }
}
