﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveGenerator : MonoBehaviour {
    
    public Color waveColor = Color.red;
    public ParticleSystem particle;
    public SpriteRenderer colorID;

    //public float MoveSpeed = 0.0f;
    public float frequency = 40.0f;  // Speed of sine movement
    public float magnitude = 0.2f;   // Size of sine movement

    private Vector3 axis;
    private Vector3 pos;

    public float shootSpeed = 10;
    public float shootTime = 1;
    public float totalShootTime = 0;

    public float dontShootTime = 1;
    public float dontTotalShootTime = 0;

    public bool shooting = true;

    void Start()
    {
        pos = transform.position;
        axis = transform.right;  // May or may not be the axis you want


        particle.startColor = waveColor;
        colorID.color = waveColor;

        particle.Play();
    }

    void Update()
    {
        particle.startColor = waveColor;
        colorID.color = waveColor;
        //pos -= transform.up * Time.deltaTime * MoveSpeed;
        transform.position = pos + axis * Mathf.Sin(Time.time * frequency) * magnitude;

        Shoot();
    }
    void Shoot()
    {
        if (shooting)
        {
            if (totalShootTime < shootTime)
                totalShootTime += Time.deltaTime;
            else
            {
                totalShootTime = 0;
                shooting = false;
                particle.startSpeed = 0;
            }
        }
        else
        {
            if (dontTotalShootTime < dontShootTime)
                dontTotalShootTime += Time.deltaTime;
            else
            {
                dontTotalShootTime = 0;
                shooting = true;
                particle.startSpeed = shootSpeed;
            }
        }
    }
}
