﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MicroAcapellaEffect : MonoBehaviour {

    public const float HalfPI = (Mathf.PI / 2);
    public bool _active = false;
    public bool _endMode = false;
    Image _image;
    float _timer = 0;
    short _trigger = 0;

	// Use this for initialization
	void Start () {
        _image = GetComponent<Image>();
        _image.color = Color.white * new Color(1,1,1,0);
       // _active = false;
    }

    public static float EaseOut(float s, int power)
    {
        var sign = power % 2 == 0 ? -1 : 1;
        return (float)(sign * (Mathf.Pow(s - 1, power) + sign));
    }

    // Update is called once per frame
    void Update () {

        if (_active)
        {
            // fade in
            if (_trigger == 0)
            {

                _timer += Time.deltaTime / 0.3f;

                float tAlpha = _timer / 0.65f;
                float t = _timer;//EaseOut(_timer, 2);//Mathf.Lerp(0,1f,(float)Mathf.Sin(_timer * HalfPI - HalfPI) + 1);

                if (tAlpha >= 1f)
                    tAlpha = 1f;


                Color color = _image.color;

                float alp = Mathf.Lerp(0, 1, tAlpha);
                float s = Mathf.Lerp(3f, 0.9f, t);

                transform.localScale = new Vector3(s, s, s);
                _image.color = new Color(1, 1, 1, alp);


                if (_timer >= 1.0)
                {
                    t = 1;
                    _timer = 0;

                    if (!_endMode)
                        _trigger++;
                    else
                        _trigger = 99;
                    transform.localScale = new Vector3(0.7349999f, 0.7349999f, 0.7349999f);
                    //_image.fillClockwise = false;
                }

                _image.fillAmount = t;

            }

            if (_trigger == 1)
            {
                _timer += Time.deltaTime / 0.8f;

                float s = Mathf.Lerp(0.9f, 0.7349999f, _timer);
                transform.localScale = new Vector3(s, s, s);

                if (_timer >= 1)
                {
                    _timer = 0;
                    _trigger = 2;
                }
            }

            if (_trigger == 2)
            {
                _timer += Time.deltaTime / 0.3f;

                float tAlpha = _timer / 0.65f;

                if (tAlpha >= 1f)
                    tAlpha = 1f;

                float tInverted = Mathf.Lerp(1, 0, _timer);

                float alp = Mathf.Lerp(1, 0, tAlpha);
                
                float s = Mathf.Lerp(0.7349999f, 0.2f, _timer);

                _image.color = new Color(1, 1, 1, alp);
                transform.localScale = new Vector3(s, s, s);

                if (_timer >= 1.0)
                {
                    tInverted = 0;
                    GameObject.Destroy(this.gameObject);
                }

                   // _image.fillAmount = tInverted;
            }

        }
	}


}
