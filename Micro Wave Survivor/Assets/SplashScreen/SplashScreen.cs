﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections.Generic;

public class SplashScreen : MonoBehaviour {

    Image _splash;
    byte _state = 0;
    int _trigger = 0;
    int _subTrigger = 0;
    float _timer = 0.0f;
    float _maxTimer = 1.0f;
    bool _bgColor = false;

    public GameObject prefabEffectAcapapella;
    public Sprite spriteSurvival;
    public Image bg;
    public Image fade;

    List<MicroAcapellaEffect> _miniPool; 

	// Use this for initialization
	void Start () {
        _splash = GetComponent<Image>();
        _splash.color = new Color(1,1,1,0) * Color.white;
        _miniPool = new List<MicroAcapellaEffect>();

    }

    void SplashPhase()
    {
        if (_timer >= 1.0f)
        {
            // current event
            switch (_trigger)
            {
                case 0:
                    { // fade in
                        Color color = _splash.color;
                        color.a += 2f * Time.deltaTime;

                        if (color.a >= 1)
                        {
                            color.a = 1.0f;
                            _trigger++;
                            _maxTimer = 2f;
                            _timer = 0;
                        }

                        _splash.color = color;



                        break;
                    }

                case 1: // fade out
                    {
                        Color color = _splash.color;
                        color.a -= 2f * Time.deltaTime;

                        if (color.a <= 0)
                        {
                            color.a = 0.0f;
                            _trigger = 0;
                            _maxTimer = 1f;
                            _timer = 0;
                            _state = 1;

                           
                        }

                        _splash.color = color;

                        break;
                    }
            }


        }
        else
        {
            _timer += Time.deltaTime / _maxTimer;
        }
    }

    void AcapellaPhase()
    {
        if (_trigger == 0)
        {
            AudioManager.PlaySound("Sounds/sfx_microwaveAcapapella", 0.7f);
            _timer = 0;
            _trigger = 1;
            float w = 1280 / 6f;
            float h = 720 / 6;
            for (int i = 0; i < 4; i++)
            {
                GameObject obj = (GameObject)GameObject.Instantiate(prefabEffectAcapapella, transform);
                MicroAcapellaEffect effect = obj.GetComponent<MicroAcapellaEffect>();

                if (i == 0)
                {
                    effect._active = true;
                    _subTrigger++;
                }
                else if (i == 2)
                {
                    effect._endMode = true;
                    obj.transform.localPosition = new Vector3(0, 95.0f, 0);
                }

                if (i <= 1)
                    obj.transform.localPosition = new Vector3(Random.Range(-w, w), Random.Range(-h, h), 0);
                else if (i == 3)
                {
                    obj.GetComponent<Image>().sprite = spriteSurvival;
                    obj.GetComponent<Image>().SetNativeSize();
                    obj.transform.localPosition = new Vector3(0, -95.0f, 0);
                }
                _miniPool.Add(effect);

            }
        }
        else if (_trigger == 1)
        {

            _timer += Time.deltaTime / 1.4f;

            if (!_bgColor)
            {
                float t = _timer;

                float a = Mathf.Lerp(0, 1, t);
                Color color = bg.color;

                if (t >= 1.0)
                {
                    color.a = 1;
                    bg.color = color;
                    _bgColor = true;
                }
                else
                {
                    color.a = Mathf.Lerp(0, 1, a);
                    bg.color = color;
                }
            }


            if (_timer >= 1.0f)
            {
                _timer = 0;
                _miniPool[_subTrigger]._active = true;
                _subTrigger++;
                if (_subTrigger >= 3)
                {
                    _trigger++;
                }
            }
        }
        else if (_trigger == 2)
        {
            _timer += Time.deltaTime / 2.5f;

            if (_timer >= 1.0f)
            {

                AudioManager.PlaySound("Sounds/sfx_survivalAcapapella", 0.7f);
                _miniPool[_subTrigger]._active = true;
                _miniPool[_subTrigger]._endMode = true;
                _trigger++;
                _timer = 0;
            }

        }
        else if (_trigger == 3)
        {
            _timer += Time.deltaTime / 3f;
            if (_timer >= 1.0f)
            {
                _timer = 0;
                _trigger++;
            }
        }
        else if (_trigger == 4)
        {
            _timer += Time.deltaTime / 0.3f;

            Color color = fade.color;

            color.a = Mathf.Lerp(0, 1, _timer);

            fade.color = color;

            if (_timer >= 1.0f)
            {

                /// MUDA DE TELA
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex +1);

                _timer = 0;
                _trigger++;
            }
        }

    }

    // Update is called once per frame
    void Update() {

        switch (_state) {
            case 0:  SplashPhase(); break;
            case 1: AcapellaPhase(); break;
        }



    }
}
