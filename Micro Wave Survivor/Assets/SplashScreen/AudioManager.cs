﻿using UnityEngine;
using System.Collections;

public class AudioManager : Singleton<AudioManager> {



    //AudioSource _secondaryMusicSource;
    AudioSource _mainSource;

    private bool _transition = false;
    private float m_gain = 0;
	private bool m_gainActive = false;
	private float m_gainTime = 0;
	private float m_oldVolume = 0;
	private float m_gainProgress = 0f;

    public override void Setup() { Init(); }

    public void Init(/*AudioSource secondaryAudioSource*/)
    {
        _mainSource = GetComponent<AudioSource>();
        //_secondaryMusicSource = secondaryAudioSource;
    }

	void Update() {

        //if (_mainSource == null)
        //    _mainSource = GetComponent<AudioSource>();

        if (!_transition)
        {
            if (m_gainActive)
            {

                m_gainProgress += Time.deltaTime / m_gainTime;

                if (m_gainProgress >= 1f)
                {
                    m_gainProgress = 1f;
                    m_gainActive = false;
                    //GetComponent<AudioSource>().volume = m_gain;
                }

                float vol = Mathf.Lerp(m_oldVolume, m_gain, m_gainProgress);

                _mainSource.volume = vol;


            }
        }
        /*else
        {
            m_gainProgress += Time.deltaTime / m_gainTime;

            float volMain = Mathf.Lerp(m_oldVolume, 0, m_gainProgress);
            float volSec = Mathf.Lerp(0, m_gain, m_gainProgress);

            _mainSource.volume = volMain;
            _secondaryMusicSource.volume = volSec;

            if (m_gainProgress >= 1f)
            {
                _mainSource.clip = _secondaryMusicSource.clip;
                _mainSource.volume = _secondaryMusicSource.volume;
                _mainSource.time = _secondaryMusicSource.time;
                _mainSource.loop = _secondaryMusicSource.loop;

                _secondaryMusicSource.clip = null;
                _mainSource.Play();
                _transition = false;
            }

        }*/
	

	}
		
	static public AudioSource PlaySound(AudioClip clip, float volume = 1f) {

			GameObject obj = new GameObject("SoundEffect " + clip.name);
			//obj.transform.parent =	transform;

			AudioSource aSource = obj.AddComponent<AudioSource>(); // add an audio source
			aSource.volume = volume;
			aSource.clip = clip; // define the clip
			aSource.Play(); // start the sound

        Destroy(obj, clip.length);

        return aSource;

			

			//Play some audio!
	}

	static public void PlaySound(string name, float volume = 1f) {

		AudioClip clip = (AudioClip)Resources.Load(name,typeof(AudioClip));
		
		GameObject obj = new GameObject("SoundEffect " + clip.name);
		//obj.transform.parent =	transform;
		
		AudioSource aSource = obj.AddComponent<AudioSource>(); // add an audio source
		aSource.volume = volume;
		aSource.clip = clip; // define the clip
		aSource.Play(); // start the sound
		
		Destroy(obj, clip.length);
		
		//Play some audio!
	}

    public AudioSource TransitionTo(AudioClip clip, float fadeDuration = 1f, float toVolume = 1f, bool loop = true)
    {
        /*   if (_secondaryMusicSource != null)
           {
               _secondaryMusicSource.clip = clip;
               _secondaryMusicSource.loop = loop;
               _secondaryMusicSource.volume = 0;
               _secondaryMusicSource.Play();

               m_oldVolume = _mainSource.volume;
               m_gain = toVolume;
               m_gainTime = fadeDuration;
               m_gainProgress = 0;
               _transition = true;

               return _secondaryMusicSource;
           }
           else
           {
               Debug.LogWarning("Error: No Secondary Audio Source");
               _transition = false;
               return null;
           }*/
        return null;
    }

	public AudioSource PlayMusic(string name, bool loop = true, float volume = 1f) {

		AudioClip clip = (AudioClip)Resources.Load(name,typeof(AudioClip));

        //Resources.As

        _mainSource.clip = clip;
        _mainSource.loop = loop;
        _mainSource.volume = volume;
        // _mainSource.enabled = true;
        _mainSource.Play();



        return _mainSource;

	}

    public AudioSource PlayMusic(AudioClip clip, bool loop = true, float volume = 1f)
    {

        _mainSource.clip = clip;
        _mainSource.loop = loop;
        _mainSource.volume = volume;
        //_mainSource.enabled = true;
        _mainSource.Play();



        return GetComponent<AudioSource>();

    }

    public void AudioGain(float gainTo, float gainTime) {

		bool dothat = true;

		m_oldVolume = _mainSource.volume;
		m_gainProgress = 0f;

		if (gainTo > 1f)
			gainTo = 1f;

		if (gainTo < 0)
			gainTo = 0;

		if (gainTime == 0) {
			dothat = false;
		} else if (gainTime < 0) {
			gainTime *= -1;
		}

		if (dothat) {
			m_gainTime = gainTime;
			m_gain = gainTo;

			m_gainActive = true;
		}

	}

	public void StopMusic() {

        _mainSource.Stop();

	}

	public void PauseMusic() {
        _mainSource.Pause();
	}

	public void UnPauseMusic() {
        _mainSource.UnPause();
	}
	
	public AudioSource MusicSource() {
	      return _mainSource;
	}
}
